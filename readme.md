# Тестовое задание

## Установка и запуск

### Создаем папку проекта и клонируем приложение
```
mkdir name_project

cd name_project

git clone https://WStanley@bitbucket.org/WStanley/testnl.git .
```

### Собираем images
#### На ПК должен быть установлен docker и docker-compose
```
make build
```

### Запускаем тесты
```
make autotest
```

### Запускаем джангу
```
make runserver
```

### Миграции и супер админ будут созданы автоматически
#### Вход в админ панель:
* логин - admin
* пароль - 12345

## Обращение к АПИ
### Список страниц
```
import requests

response = requests.get('http://0.0.0.0:8000/api/v1/pages/')

response.status_code == 200

response.json()

{
    "count": 3,
    "next": None,
    "previous": None,
    "results": [
        {
            "slug": "paren-banda-novyj-solnce-osnovanie",
            "title": "Парень банда новый солнце основание.",
            "absolute_url": "/pages/page_detail/paren-banda-novyj-solnce-osnovanie/"
        },
        {
            "slug": "plavno-skolzit-moneta-osnovanie-prezhde",
            "title": "Плавно скользить монета основание прежде.",
            "absolute_url": "/pages/page_detail/plavno-skolzit-moneta-osnovanie-prezhde/"
        },
        {
            "slug": "magazin-drognut-pesenka-stavit-nizkij",
            "title": "Магазин дрогнуть песенка ставить низкий.",
            "absolute_url": "/pages/page_detail/magazin-drognut-pesenka-stavit-nizkij/"
        }
    ]
}

```

### Страница
```
import requests

response = requests.get('http://0.0.0.0:8000/api/v1/pages/paren-banda-novyj-solnce-osnovanie/')

response.status_code == 200

response.json()

{
    "title": "Парень банда новый солнце основание.",
    "slug": "paren-banda-novyj-solnce-osnovanie",
    "contents": [
        {
            "title": "Ученый порядок правый один остановить.",
            "counter": 2,
            "file": "/media/audio/uchenyj-poryadok-pravyj-odin-ostanovit.wav",
            "bitrate": 320,
            "position": 1
        },
        {
            "title": "Место выражаться тревога демократия передо.",
            "counter": 2,
            "file": "/media/video/mesto-vyrazhatsya-trevoga-demokratiya-peredo.mp4",
            "subtitles": "/media/video/mesto-vyrazhatsya-trevoga-demokratiya-peredo_subtitles.txt",
            "position": 1
        },
        {
            "title": "Песенка запустить передо банк остановить.",
            "counter": 2,
            "file": "/media/audio/pesenka-zapustit-peredo-bank-ostanovit.wav",
            "bitrate": 256,
            "position": 2
        },
        {
            "title": "Скользить число миг мучительно запретить.",
            "counter": 2,
            "file": "/media/video/skolzit-chislo-mig-muchitelno-zapretit.mp4",
            "subtitles": "/media/video/skolzit-chislo-mig-muchitelno-zapretit_subtitles.txt",
            "position": 2
        }
    ]
}
```
