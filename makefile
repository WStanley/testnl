build:
	docker-compose build --pull
down:
	docker-compose down
runserver:
	docker-compose up runserver
autotest:
	docker-compose up autotest
prune:
	docker container prune
# 	docker rmi -f $(docker images -a -q)
# 	docker kill $(docker ps -q)