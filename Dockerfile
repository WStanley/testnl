FROM python:3.8.3-alpine

RUN pip install --upgrade pip

WORKDIR /app

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

RUN apk update \
    && apk add python3-dev gcc musl-dev \
    && apk add postgresql-dev \
    && apk add jpeg-dev zlib-dev libjpeg

COPY ./requirements.txt /app
RUN pip install -r requirements.txt

COPY . /app
RUN mv wait-for /bin/wait-for

EXPOSE 8000
