from django.contrib import admin

from pages.models import Audio, Page, Text, Video, Content

admin.site.register(Audio)
admin.site.register(Text)
admin.site.register(Video)


class AudioInline(admin.TabularInline):

    fields = ('position', 'title', 'file', 'bitrate', 'counter')
    readonly_fields = ('counter', )

    model = Audio


class TextInline(admin.TabularInline):

    fields = ('position', 'title', 'article', 'counter')
    readonly_fields = ('counter', )

    model = Text


class VideoInline(admin.TabularInline):

    fields = ('position', 'title', 'file', 'subtitles', 'counter')
    readonly_fields = ('counter', )

    model = Video


@admin.register(Page)
class PageAdmin(admin.ModelAdmin):

    list_display = ('title', 'slug')
    fields = ('title', 'slug')

    search_fields = ['title']

    inlines = [
        AudioInline,
        TextInline,
        VideoInline
    ]

    def get_search_results(self, request, queryset, search_term):
        queryset, may_have_duplicates = super().get_search_results(
            request, queryset, search_term,
        )

        # Добавляю к найденым страницы у которых в заголовках контента тоже
        # есть совпадения
        # По тексту у контента текст пришлось использовать contains,
        # т.к. сkeditor добавляет html теги в начале текста
        queryset2 = Page.objects.search_pages_by_content(search_term)

        queryset = queryset.union(queryset2)

        return queryset, may_have_duplicates