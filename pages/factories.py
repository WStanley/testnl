import factory.fuzzy

from pages.models import Audio, Content, Page, Text, Video


class PageFactory(factory.django.DjangoModelFactory):

    title = factory.Faker('text', max_nb_chars=20)

    class Meta:
        model = Page


class ContentFactory(factory.django.DjangoModelFactory):

    title = factory.Faker('text', max_nb_chars=20)
    counter = factory.Faker('pyint', min_value=0, max_value=500)
    position = factory.Sequence(lambda n: n)
    page = factory.SubFactory(PageFactory)

    class Meta:
        model = Content


class AudioFactory(ContentFactory):

    file = factory.django.FileField()
    bitrate = factory.Faker('pyint', min_value=500, max_value=1240)

    class Meta:
        model = Audio


class VideoFactory(ContentFactory):

    file = factory.django.FileField()
    subtitles = factory.django.FileField()

    class Meta:
        model = Video


class TextFactory(ContentFactory):

    article = factory.Faker('text')

    class Meta:
        model = Text
