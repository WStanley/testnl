from ckeditor.fields import RichTextField
from django.core.validators import FileExtensionValidator
from django.db import models
from django.db.models import Q
from django.db.models.signals import pre_delete
from django.dispatch import receiver
from django.urls import reverse
from django.utils.translation import gettext_lazy as _

from utils.text import translite


class PageManager(models.Manager):
    """Расширяем менеджер для страницы"""

    def search_pages_by_content(self, search_term):
        """Расширяю поиск в админке"""
        return (
            self.filter(
                Q(title__istartswith=search_term) |
                Q(audio__title__istartswith=search_term) |
                Q(video__title__istartswith=search_term) |
                Q(text__title__istartswith=search_term)
            ).distinct()
        )


class Page(models.Model):
    """Страница с контентом"""

    title = models.CharField(max_length=250, unique=True)
    slug = models.SlugField(max_length=255, unique=True, blank=True)

    objects = PageManager()

    class Meta:
        verbose_name = _('page')
        verbose_name_plural = _('pages')

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = translite(self.title)
        super().save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse("pages:pages-detail", kwargs={"slug": self.slug})


class Content(models.Model):
    """Общее для всех страниц с контентом"""

    title = models.CharField(max_length=150, unique=True)
    counter = models.PositiveIntegerField(default=0)
    position = models.PositiveSmallIntegerField(default=0)
    page = models.ForeignKey(
        "pages.Page", null=True, blank=True, on_delete=models.SET_NULL
    )

    class Meta:
        abstract = True


class Video(Content):
    """Video контент"""

    file = models.FileField(
        upload_to='video/', max_length=180,
        validators=[
            FileExtensionValidator(
                allowed_extensions=['webm', 'avi', 'mp4', 'mov']
            )
        ],
    )
    subtitles = models.FileField(
        upload_to='subtitles/', max_length=180,
        validators=[
            FileExtensionValidator(
                allowed_extensions=['txt']
            )
        ],
    )

    class Meta:
        verbose_name = _('video')
        verbose_name_plural = _('video')

    def __str__(self):
        return self.title


class Audio(Content):
    """Аудио контент"""

    file = models.FileField(
        upload_to='audio/',
        validators=[
            FileExtensionValidator(allowed_extensions=['mp3', 'wav'])
        ],
    )
    bitrate = models.PositiveIntegerField()

    class Meta:
        verbose_name = _('audio')
        verbose_name_plural = _('audio')

    def __str__(self):
        return self.title


class Text(Content):
    """Text контент"""

    article = RichTextField()

    class Meta:
        verbose_name = _('text')
        verbose_name_plural = _('text')
        ordering = ['position']

    def __str__(self):
        return self.title


@receiver(pre_delete, sender=Audio)
def audio_clean(sender, instance, **kwargs):
    """Удаляем файл с диска"""
    if instance.file:
        instance.file.delete()


@receiver(pre_delete, sender=Video)
def video_clean(sender, instance, **kwargs):
    """Удаляем файлы с диска"""
    if instance.file:
        instance.file.delete()
    if instance.subtitles:
        instance.subtitles.delete()
