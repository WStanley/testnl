from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase

from pages.factories import (
    AudioFactory, PageFactory, TextFactory,
    VideoFactory,
)
from utils.text import translite


class ApiViewTest(APITestCase):
    """Тестируем апи"""

    def test_detail(self) -> None:
        """Проверяем получение страницы"""

        page_title = 'Заголовок'
        count_audio = 2
        count_video = 2
        count_text = 2

        page = PageFactory(title=page_title)
        AudioFactory.create_batch(count_audio, page=page)
        TextFactory.create_batch(count_text, page=page)
        VideoFactory.create_batch(count_video, page=page)

        url = reverse('pages:pages-detail', kwargs={'slug': page.slug})

        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['title'], page_title)
        self.assertEqual(response.data['slug'], translite(page_title))
        self.assertEqual(len(response.data['audio']), count_audio)
        self.assertEqual(len(response.data['video']), count_video)
        self.assertEqual(len(response.data['text']), count_text)

    def test_list(self) -> None:
        """Проверяем получение списка"""
        url = reverse('pages:pages-list')

        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['count'], 25)
