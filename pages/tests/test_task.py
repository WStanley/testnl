from django.db.models import Sum
from django.test import TestCase

from pages.models import Page
from pages.tasks import add_counter


class TestTask(TestCase):
    """Тестируем таски"""

    @staticmethod
    def get_counter_sum(page):
        """Считаем количество контента и сумму просмотров"""
        page.refresh_from_db()

        sum_counter_audio = page.audio_set.aggregate(Sum('counter'))
        sum_counter_video = page.video_set.aggregate(Sum('counter'))
        sum_counter_text = page.text_set.aggregate(Sum('counter'))
        all_counter_sum = (
            sum_counter_audio['counter__sum'] +
            sum_counter_video['counter__sum'] +
            sum_counter_text['counter__sum']
        )

        count_audio = page.audio_set.count()
        count_video = page.video_set.count()
        count_text = page.text_set.count()
        all_count = count_audio + count_video + count_text
        return all_counter_sum, all_count

    def test_task_counter(self):
        page = Page.objects.first()
        save_all_counter_sum, all_count = self.get_counter_sum(page)

        add_counter(page.id)

        new_all_counter_sum, _ = self.get_counter_sum(page)

        self.assertEqual(save_all_counter_sum + all_count, new_all_counter_sum)
