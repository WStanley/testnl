from celery import shared_task
from django.db.models import F
from pages.models import Page


@shared_task
def add_counter(page_id):
    """Увеличиваем счетчик просмотров контента"""
    try:
        page = Page.objects.get(pk=page_id)
    except Page.DoesNotExist:
        return

    page.audio_set.update(counter=F('counter') + 1)
    page.video_set.update(counter=F('counter') + 1)
    page.text_set.update(counter=F('counter') + 1)
