from rest_framework import serializers

from pages.models import Audio, Page, Text, Video


class TextSerializer(serializers.ModelSerializer):

    class Meta:
        model = Text
        fields = ['title', 'article', 'counter', 'position']


class AudioSerializer(serializers.ModelSerializer):

    class Meta:
        model = Audio
        fields = ['title', 'bitrate', 'file', 'position', 'counter']


class VideoSerializer(serializers.ModelSerializer):

    class Meta:
        model = Video
        fields = ['title', 'counter', 'file', 'subtitles', 'position']


class PageDetailSerializer(serializers.ModelSerializer):

    audio = AudioSerializer(many=True, source='audio_set')
    video = VideoSerializer(many=True, source='video_set')
    text = TextSerializer(many=True, source='text_set')

    class Meta:
        model = Page
        lookup_field = 'slug'
        extra_kwargs = {
            'url': {'lookup_field': 'slug'}
        }
        fields = (
            'title',
            'slug',
            'audio',
            'video',
            'text',
        )


class PagesSerializer(serializers.ModelSerializer):

    absolute_url = serializers.CharField(
        source='get_absolute_url', read_only=True
    )

    class Meta:
        model = Page
        lookup_field = 'slug'
        extra_kwargs = {
            'url': {'lookup_field': 'slug'}
        }
        fields = (
            'slug',
            'title',
            'absolute_url'
        )
