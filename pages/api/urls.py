from rest_framework import routers

from pages.api.views import PagesViewSet

app_name = 'pages'


router = routers.SimpleRouter()

router.register(r'pages', PagesViewSet, basename='pages')

urlpatterns = router.urls
