from rest_framework import mixins, viewsets
from rest_framework.response import Response

from pages.api.serializers import PageDetailSerializer, PagesSerializer
from pages.models import Page


from pages.tasks import add_counter


class PagesViewSet(
    mixins.ListModelMixin,
    mixins.RetrieveModelMixin,
    viewsets.GenericViewSet
):

    queryset = Page.objects.all()
    serializer_class = PagesSerializer

    lookup_field = 'slug'

    def get_serializer_class(self):
        serializers = {
            'retrieve': PageDetailSerializer,
        }
        return serializers.get(self.action, PagesSerializer)

    def retrieve(self, request, *args, **kwargs):
        page = self.get_object()

        add_counter.delay(page.id)

        serializer = self.get_serializer(page)
        return Response(serializer.data)
